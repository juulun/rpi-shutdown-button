#!/usr/bin/python3
import sys

from gpiozero import LED, Button
from subprocess import call
from time import time, sleep
from signal import pause

# pushbutton connected to this GPIO pin, using pin 5 also has the benefit of
# waking / powering up Raspberry Pi when button is pressed
shutdown_pin = None
led_pin = None
led = None

# if button pressed for at least this long then shut down. if less then reboot. in milliseconds.
shutdown_threshold = 1000
button_pressed_time = None


def get_timestamp():
    return int(time() * 1000.0)


def button_pressed():
    global button_pressed_time
    button_pressed_time = get_timestamp()


def button_released():
    global button_pressed_time

    if button_pressed_time is not None:
        elapsed = (get_timestamp() - button_pressed_time)
        button_pressed_time = None
        if elapsed >= shutdown_threshold:
            # button pressed for more than specified time, shutdown
            print("Button was pressed long, shutting down")
            if led is not None:
                led.off()
                sleep(0.2)
                led.on()
                sleep(0.7)
                led.off()
                sleep(0.2)
                led.on()
                sleep(0.7)
            try:
                call(['shutdown', '-h', 'now'], shell=False)
            except FileNotFoundError:
                print("Shutdown command not found, you are probably not running this script as root")
        else:
            # button pressed for a shorter time, reboot
            print("Button was pressed short, rebooting")
            if led is not None:
                led.off()
                sleep(0.2)
                led.on()
                sleep(0.4)
                led.off()
                sleep(0.2)
                led.on()
                sleep(0.4)
            try:
                call(['shutdown', '-r', 'now'], shell=False)
            except FileNotFoundError:
                print("Shutdown command not found, you are probably not running this script as root")


# Get iterator for command line arguments and skip first item (script call)
arg_it = iter(sys.argv)
next(arg_it)

# Parse command line arguments
for arg in arg_it:
    if arg == '--button-pin' or arg == '-b' or arg == '--button':
        try:
            shutdown_pin = int(next(arg_it))
        except:
            print("Shutdown button PIN has to be an integer")
            exit(1)

    elif arg == '--led-pin' or arg == '-l' or arg == '--led':
        try:
            led_pin = int(next(arg_it))
        except:
            print("LED PIN has to be an integer")
            exit(1)

    elif arg == '-t' or arg == '--threshold':
        try:
            shutdown_threshold = int(next(arg_it))
        except:
            print("Threshold for shutdown instead of reboot has to be an integer")
            exit(1)

    elif arg == '-h':
        print("Usage:", sys.argv[0], "--button <button pin #> --led <LED pin #> --threshold <milliseconds> ")
        exit()

    else:
        print("Unknown argument:", arg)
        print("Use \'", sys.argv[0], " -h\' to print available arguments.")
        exit(1)

if shutdown_pin is None:
    print("Shutdown button PIN has to be set")
    exit(1)

if led_pin is not None:
    led = LED(led_pin)
    led.on()

# subscribe to button presses
button = Button(shutdown_pin)
button.when_pressed = button_pressed
button.when_released = button_released

pause()
