# rpi-shutdown-button

Connect a button to the RPi and shut it down when the button is pressed.

Supports a led that is powered when the service is running and blinks when the shutdown is initiated.

Install by running the install script in the `service` directory.

# Power LEDs
Not yet supported, read something about this [here](https://indibit.de/raspberry-pi-externe-status-leds-ueber-gpio-pwr_led-act_led/).
