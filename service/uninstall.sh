#!/bin/bash
systemctl stop shutdown-button
systemctl disable shutdown-button
rm /usr/local/bin/button-shutdown.py
rm /etc/systemd/system/shutdown-button.service