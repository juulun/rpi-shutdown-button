#!/bin/bash
if ! which python3 &> /dev/null || ! which pip3 &> /dev/null; then
	echo "python3 and pip3 are required for this service"
	exit 1
fi

# Requires root privileges
if [[ "$UID" != 0 ]]; then
    echo "Please run this installer as root..."
    exit 2
fi

# Primitive check to determine the current directory
if [[ ! -e ./install.sh ]]; then
    echo "Please execute this script from the service directory..."
    exit 3
fi

echo "Which PIN is the shutdown button connected to?"
read button_pin

echo "Which PIN is the LED connected to? (Leave empty if there is no LED)"
read led_pin

if [ -z "$button_pin" ]; then
    echo "PIN number for button is required"
    exit 1
fi

pip3 install -r ../requirements.txt

cp ../button-shutdown.py /usr/local/bin
cp ./shutdown-button.service /etc/systemd/system

chown root:root /usr/local/bin/button-shutdown.py
chown root:root /etc/systemd/system/shutdown-button.service

sed -i "s/<button-placeholder>/--button $button_pin/g" /etc/systemd/system/shutdown-button.service
if [ -n "$led_pin" ]; then
    sed -i "s/<led-placeholder>/--led $led_pin/g" /etc/systemd/system/shutdown-button.service
else
    sed -i "s/<led-placeholder>//g" /etc/systemd/system/shutdown-button.service
fi

echo "This service can be started using 'systemctl enable shutdown-button.service'"
